# StopWatch by Supanat Pokturng (5710546429) #

I ran the tasks on my laptop 3 times and calculate the average, and these are the results : 


```
#!java

                     Task                       |     Elapsed Time (Second)
----------------------------------------------- | -------------------------------
TestAppendToString (Length = 100,000)           |            9.227611
TestAppendToStringBuilder (Length = 100,000)    |            0.007323
TestSumDoublePrimitive (Count = 100,000,000)    |            0.245380
TestSumDouble (Count = 100,000,000)             |            1.418268 
TestSumBigDecimal (Count = 100,000,000)         |            2.430657
```



# Why StringBuilder is faster than String ? #

String is an immutable class. So, when you want to append a word to a word, they always create a new object (not the same object). They just use more time than StringBuilder that is not create a new object but they use own object.

# Why primitive data type (double) is faster than object(Double,BigDecimal) ? #

We will define Double and BigDecimal to an object that have a double inside. When you want to change a value, they have to pass the outside that use more time. So Double and BigDecimal are slower than double.