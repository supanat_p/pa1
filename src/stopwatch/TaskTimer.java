/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package stopwatch;

/**
 * TaskTimer use to execute each task
 * and measures the time and print.
 * @author Supanat Pokturng
 * @version 2015.02.01
 */
public class TaskTimer {
	
	/**
	 * Measures and print the result of each task.
	 * @param task is any task that execute
	 */
	public void measureAndPrint(Runnable task) {
		
		/** Declare a timer to get an elapsed time. */
		StopWatch timer = new StopWatch();
		
		System.out.print(task.toString());
		timer.start();
		task.run();
		timer.stop();
		System.out.printf("Elapsed time %.6f sec\n\n", timer.getElapsed());
	}
}
