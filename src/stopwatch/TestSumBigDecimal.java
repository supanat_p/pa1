/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package stopwatch;

import java.math.BigDecimal;

/**
 * Task5 : Summation of the big decimal.
 * @author Supanat Pokturng
 * @version 2015.02.01
 */
public class TestSumBigDecimal implements Runnable {
	/** Declare array of Bigdecimal. */
	private BigDecimal[] values;
	
	/**Round for loop.*/
	private int counter;
	
	/**Declare a size of array.*/
	private static final int ARRAY_SIZE = 500000;
	
	/**
	 * Set the round of the loop.
	 * @param count is round of the loop
	 */
	public void setCounter(int count) {
		this.counter = count;
	}
	
	/**
	 * Get a description of this class.
	 * @return String of a description
	 */
	public String toString() {
		return String.format("Sum array of BigDecimal with count=%,d\n",counter);
	}
	
	/**
	 * Constructor and initial value in array.
	 * @param count is round of the loop
	 */
	public TestSumBigDecimal(int count){
		setCounter(count);
		values = new BigDecimal[ARRAY_SIZE];
		for (int i = 0; i < ARRAY_SIZE; i++)
			values[i] = new BigDecimal(i + 1);
	}
	
	/**
	 * Use to execute the program.
	 */
	public void run() {
		BigDecimal sum = new BigDecimal(0.0);
		
		for (int count = 0 , i = 0 ; count < counter ; count++ , i++) {
			if (i >= ARRAY_SIZE)
				i = 0;
			sum = sum.add(values[i]);
		}
		System.out.println("sum = " + sum);
	}
}
