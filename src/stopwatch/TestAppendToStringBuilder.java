/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package stopwatch;

/**
 * Task2 : Append char to StringBuilder.
 * @author Supanat Pokturng
 * @version 20155.02.01
 */
public class TestAppendToStringBuilder implements Runnable {
	
	/** Round of the loop. */
	private int counter;
	
	/** character that append to StringBuilder. */
	private final char character = 'a';
	
	/**
	 * Constructor of this class.
	 * @param count is round of the loop
	 */
	public TestAppendToStringBuilder(int count) {
		setCounter(count);
	}
	
	/**
	 * Set the round of the loop.
	 * @param count is round of the loop
	 */
	public void setCounter(int count) {
		this.counter = count;
	}
	
	/**
	 * Get a description of this class.
	 * @return String of a description
	 */
	public String toString() {
		return String.format("Append to StringBuilder with count=%,d\n", counter);
	}
	
	/**
	 * Use to execute the program.
	 */
	public void run() {
		StringBuilder builder = new StringBuilder(); 
		int k = 0;
		while(k++ < counter) {
			builder = builder.append(this.character);
		}
		// now create a String from the result, to be compatible with task 1.
		String result = builder.toString();
		System.out.println("final string length = " + result.length());
	}
}
