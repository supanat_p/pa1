package stopwatch;

import java.util.Scanner;

public class StopWatchTest {
	public static void main(String [] a){
		StopWatch timer = new StopWatch( );
		System.out.println("Starting task");
		timer.start( );
		for(int i=0 ; i<10 ; i++) System.out.println(i+1);
		System.out.printf("elapsed = %.6f sec\n", timer.getElapsed() );
		if ( timer.isRunning() ) System.out.println("timer is running");
		else System.out.println("timer is stopped");
		timer.stop( ); // stop timing the work
		System.out.printf("elapsed = %.6f sec\n", timer.getElapsed() );
		if ( timer.isRunning() ) System.out.println("timer is running");
		else System.out.println("timer is stopped");
		System.out.printf("elapsed = %.3f sec\n", timer.getElapsed() );
		
	}
}
