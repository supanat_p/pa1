/*
 * This source code is Copyright 2015 by Supanat Pokturng
 */
package stopwatch;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;
/**
 * Main class that use to test the time that each task used.
 * @author Supanat Pokturng
 * @version 2015.02.1
 */
public class Main {
	/**
	 * This is main method that declare task and execute.
	 * @param args is the arguments in the method
	 * @throws IOException is throw the exception
	 */
	public static void main(String[] args) throws IOException {
		/** Declare and initial task's array. */
		Runnable[] tasks = new Runnable[]{
			new TestAppendToString(100000), 
			new TestAppendToStringBuilder(100000),
			new TestSumDoublePrimitive(100000000),
			new TestSumDouble(100000000),
			new TestSumBigDecimal(100000000)
		};
		/** Declare a TaskTimer to execute the task. */
		TaskTimer taskTimer = new TaskTimer();
		for(int i=0 ;i<tasks.length ; i++) {
			taskTimer.measureAndPrint(tasks[i]);
		}
	}
}