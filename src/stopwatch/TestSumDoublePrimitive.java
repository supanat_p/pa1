/**
 * This  source code is Copyright 2015 by Supanat Pokturng.
 */
package stopwatch;

/**
 * Task3 : Summation of double with primitive.
 * @author Supanat Pokturng
 * @version 2015.02.01
 */
public class TestSumDoublePrimitive implements Runnable {
	
	/** Declare array of double. */
	private double[] values;
	
	/** Round of the loop. */
	private int counter;
	
	/** Size of the array. */
	private static final int ARRAY_SIZE = 50000;
	
	/**
	 * Set the round of the loop.
	 * @param count is round of the loop.
	 */
	public void setCounter(int count) {
		this.counter = count;
	}
	
	/**
	 * Get a description of this class.
	 * @return String of a description
	 */
	public String toString() {
		return String.format("Sum array of double primitives with count=%,d\n", counter);
	}
	
	/**
	 * Constructor and initial the value in array.
	 * @param count is round of the loop.
	 */
	public TestSumDoublePrimitive(int count) {
		setCounter(count);
		
		values = new double[ARRAY_SIZE];
		for(int i=0 ; i < ARRAY_SIZE ; i++) {
			values[i] = i+1; 
		}
	}
	
	/**
	 * Use to execute the program.
	 */
	public void run() {
		double sum = 0.0;
		// count = loop counter, i = array index value
		for(int count=0, i=0; count<counter; count++, i++) {
			if (i >= ARRAY_SIZE) 
				i = 0;
			sum = sum + values[i];
		}
		System.out.println("sum = " + sum);
	}
}
