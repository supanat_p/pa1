/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package stopwatch;

/**
 * Task1 : Append char to String.
 * @author Supanat Pokturng
 * @version 2015.02.01
 */
public class TestAppendToString implements Runnable {
	
	/** round of the loop. */
	private int counter;
	
	/** character that append to String. */
	private final char character = 'a';
	
	/**
	 * Constructor of the class.
	 * @param count is round of loop
	 */
	public TestAppendToString(int count) {
		setCounter(count);
	}
	
	/**
	 * Set the round of the loop.
	 * @param count is round of loop
	 */
	public void setCounter(int count) {
		this.counter = count;
	}
	
	/**
	 * use to execute the program.
	 */
	public void run() {
		String sum = ""; 
		int k = 0;
		while(k++ < counter) {
			sum = sum + this.character;
		}
		System.out.println("final string length = " + sum.length());	
	}
		
	/**
	 * Get a description of this class.
	 * @return String of the description
	 */
	public String toString() {
		return  String.format("Append to String with count=%,d\n", counter);
	}
	
}
